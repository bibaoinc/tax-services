package com.bibao.tax.biz;

import com.bibao.tax.model.TaxWorkingSheet;

public interface FederalTaxService {
	public void calculate(TaxWorkingSheet workingSheet);
}
