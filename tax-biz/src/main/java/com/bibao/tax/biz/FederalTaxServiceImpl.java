package com.bibao.tax.biz;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bibao.tax.model.TaxWorkingSheet;
import com.bibao.tax.persistence.dao.FederalTaxDao;
import com.bibao.tax.persistence.entity.FederalTaxEntity;

@Service
public class FederalTaxServiceImpl implements FederalTaxService {
	@Autowired
	private FederalTaxDao taxDao;
	
	@Override
	public void calculate(TaxWorkingSheet workingSheet) {
		String filingType = workingSheet.getRequest().getFilingType().name();
		List<FederalTaxEntity> entities = taxDao.findTaxRatesByType(filingType);
		Collections.sort(entities, 
				(e1, e2) -> e1.getIncomeFrom().compareTo(e2.getIncomeFrom()));
		double federalTax = 0;
		double taxableIncome = workingSheet.getTaxableIncome();
		for (FederalTaxEntity e: entities) {
			double incomeFrom = e.getIncomeFrom().doubleValue();
			double incomeTo = e.getIncomeTo()!=null? e.getIncomeTo().doubleValue() : Double.MAX_VALUE;
			if (taxableIncome>incomeTo) {
				federalTax += (incomeTo - incomeFrom) * e.getTaxRate();
			} else {
				federalTax += (taxableIncome - incomeFrom) * e.getTaxRate();
				break;
			}
		}
		workingSheet.setFederalTax(federalTax);
	}

}
