package com.bibao.tax.biz;

import com.bibao.tax.model.TaxWorkingSheet;

public interface SocialSecurityService {
	public void calculate(TaxWorkingSheet workingSheet);
}
