package com.bibao.tax.biz;

import org.springframework.stereotype.Service;

import com.bibao.tax.model.TaxWorkingSheet;
import com.bibao.tax.model.enums.TaxFilingType;

@Service
public class SocialSecurityServiceImpl implements SocialSecurityService {
	private static final double MAX_SS = 120000;
	
	@Override
	public void calculate(TaxWorkingSheet workingSheet) {
		double grossIncome = workingSheet.getRequest().getGrossIncome();
		double deduction = getDeduction(workingSheet.getRequest().getFilingType());
		workingSheet.setDeduction(deduction);
		double taxableIncome = grossIncome>=deduction? grossIncome - deduction : 0;
		workingSheet.setTaxableIncome(taxableIncome);
		if (taxableIncome>=MAX_SS) workingSheet.setSocialSecurity(MAX_SS * 0.062);
		else workingSheet.setSocialSecurity(taxableIncome * 0.062);
		workingSheet.setMedicare(taxableIncome * 0.0145);
	}

	private double getDeduction(TaxFilingType filingType) {
		double deduction = 0;
		switch (filingType) {
		case SINGLE: deduction = 12000; break;
		case MARRIED: deduction = 24000; break;
		default: break;
		}
		return deduction;
	}
}
