package com.bibao.tax.biz;

import com.bibao.tax.model.TaxWorkingSheet;

public interface StateTaxService {
	public void calculate(TaxWorkingSheet workingSheet);
}
