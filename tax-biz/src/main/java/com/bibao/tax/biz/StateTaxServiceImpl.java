package com.bibao.tax.biz;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bibao.tax.model.StateIncome;
import com.bibao.tax.model.StateTax;
import com.bibao.tax.model.TaxWorkingSheet;
import com.bibao.tax.persistence.dao.StateTaxDao;
import com.bibao.tax.persistence.entity.StateTaxEntity;

@Service
public class StateTaxServiceImpl implements StateTaxService {
	@Autowired
	private StateTaxDao taxDao;
	
	@Override
	public void calculate(TaxWorkingSheet workingSheet) {
		List<String> states = workingSheet.getRequest().getStateIncomes()
								.stream().map(StateIncome::getState).collect(Collectors.toList());
		List<StateTaxEntity> entities = taxDao.findByStates(states);
		Map<String, Double> taxMap = 
				entities.stream()
				.collect(Collectors.toMap(StateTaxEntity::getState, StateTaxEntity::getTaxRate));
		List<StateTax> stateTaxList = new ArrayList<>();
		for (StateIncome income: workingSheet.getRequest().getStateIncomes()) {
			StateTax stateTax = new StateTax();
			String state = income.getState();
			stateTax.setState(state);
			stateTax.setTax(income.getIncome() * taxMap.get(state));
			stateTaxList.add(stateTax);
		}
		workingSheet.setStateTaxes(stateTaxList);
	}

}
