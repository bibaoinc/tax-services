package com.bibao.tax.biz;

import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bibao.tax.model.StateTax;
import com.bibao.tax.model.TaxRequest;
import com.bibao.tax.model.TaxResponse;
import com.bibao.tax.model.TaxWorkingSheet;
import com.bibao.tax.model.exceptions.IllegalIncomeException;
import com.bibao.tax.model.exceptions.NoStateIncomeException;

@Service
public class TaxCalculator {
	@Autowired
	private FederalTaxService federalTaxService;
	
	@Autowired
	private StateTaxService stateTaxService;
	
	@Autowired
	private SocialSecurityService socialSecurityService;
	
	public TaxResponse calculate(TaxRequest request) {
		validate(request);
		TaxWorkingSheet workingSheet = new TaxWorkingSheet();
		workingSheet.setRequest(request);
		socialSecurityService.calculate(workingSheet);
		federalTaxService.calculate(workingSheet);
		stateTaxService.calculate(workingSheet);
		TaxResponse response = new TaxResponse();
		response.setName(workingSheet.getRequest().getName());
		response.setSsn(workingSheet.getRequest().getSsn());
		response.setFederalTax(workingSheet.getFederalTax());
		response.setGrossIncome(workingSheet.getRequest().getGrossIncome());
		response.setMedicare(workingSheet.getMedicare());
		response.setSocialSecurity(workingSheet.getSocialSecurity());
		response.setStateTax(workingSheet.getStateTaxes()
				.stream().collect(Collectors.summingDouble(StateTax::getTax)));
		calculateNetIncome(response);
		return response;
	}
	
	private void calculateNetIncome(TaxResponse response) {
		double netIncome = response.getGrossIncome() - response.getFederalTax()
							- response.getStateTax() - response.getSocialSecurity()
							- response.getMedicare();
		response.setNetIncome(netIncome);
	}
	
	private void validate(TaxRequest request) {
		Objects.requireNonNull(request, "TaxRequest cannot be null");
		Objects.requireNonNull(request.getFilingType(), "TaxFilingType cannot be null");
		if (request.getGrossIncome()<0) {
			throw new IllegalIncomeException("The gross income cannot be negative");
		}
		if (CollectionUtils.isEmpty(request.getStateIncomes())) {
			throw new NoStateIncomeException("At least one state tax income is required");
		}
	}

	public void setSocialSecurityService(SocialSecurityService socialSecurityService) {
		this.socialSecurityService = socialSecurityService;
	}
	
}
