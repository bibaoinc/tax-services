package com.bibao.tax.biz.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.bibao.tax.biz")
public class TaxServiceBizConfig {
	
}
