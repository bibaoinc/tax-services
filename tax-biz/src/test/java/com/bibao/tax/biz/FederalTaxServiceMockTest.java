package com.bibao.tax.biz;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import com.bibao.tax.model.TaxRequest;
import com.bibao.tax.model.TaxWorkingSheet;
import com.bibao.tax.model.enums.TaxFilingType;
import com.bibao.tax.persistence.dao.FederalTaxDao;
import com.bibao.tax.persistence.entity.FederalTaxEntity;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class FederalTaxServiceMockTest {
	@Mock
	private FederalTaxDao taxDao;
	
	@InjectMocks
	private FederalTaxServiceImpl service;
	
	@Test
	public void testCalculate() {
		TaxWorkingSheet workingSheet = createWorkingSheet();
		List<FederalTaxEntity> entities = createEntities();
		when(taxDao.findTaxRatesByType(anyString())).thenReturn(entities);
		
		workingSheet.setTaxableIncome(8000);
		service.calculate(workingSheet);
		assertEquals(800, workingSheet.getFederalTax(), 0.0001);
		
		workingSheet.setTaxableIncome(25000);
		service.calculate(workingSheet);
		assertEquals(4000, workingSheet.getFederalTax(), 0.0001);
		
		workingSheet.setTaxableIncome(52000);
		service.calculate(workingSheet);
		assertEquals(11600, workingSheet.getFederalTax(), 0.0001);
		
		workingSheet.setTaxableIncome(100000);
		service.calculate(workingSheet);
		assertEquals(28000, workingSheet.getFederalTax(), 0.0001);
	}
	
	private TaxWorkingSheet createWorkingSheet() {
		TaxWorkingSheet workingSheet = new TaxWorkingSheet();
		TaxRequest request = new TaxRequest();
		request.setFilingType(TaxFilingType.SINGLE);
		workingSheet.setRequest(request);
		return workingSheet;
	}
	
	private List<FederalTaxEntity> createEntities() {
		List<FederalTaxEntity> entities = new ArrayList<>();
		entities.add(buildEntity(0, 10000, 0.1));
		entities.add(buildEntity(80000, -1, 0.4));
		entities.add(buildEntity(10000, 30000, 0.2));
		entities.add(buildEntity(30000, 80000, 0.3));
		return entities;
	}
	
	private FederalTaxEntity buildEntity(double from, double to, double rate) {
		FederalTaxEntity entity = new FederalTaxEntity();
		entity.setIncomeFrom(BigDecimal.valueOf(from));
		entity.setIncomeTo(BigDecimal.valueOf(to));
		if (to<0) entity.setIncomeTo(null);
		entity.setTaxRate(rate);
		return entity;
	}
}
