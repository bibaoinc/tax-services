package com.bibao.tax.biz;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.bibao.tax.model.TaxRequest;
import com.bibao.tax.model.TaxWorkingSheet;
import com.bibao.tax.model.enums.TaxFilingType;

public class SocialSecurityServiceTest {
	private SocialSecurityServiceImpl service;
	
	@BeforeEach
	public void setup() {
		service = new SocialSecurityServiceImpl();
	}
	
	@Test
	public void testCalculate() {
		TaxWorkingSheet workingSheet = new TaxWorkingSheet();
		workingSheet.setRequest(new TaxRequest());
		
		// Single and Low Income
		workingSheet.getRequest().setFilingType(TaxFilingType.SINGLE);
		workingSheet.getRequest().setGrossIncome(50000);
		service.calculate(workingSheet);
		assertEquals(12000, workingSheet.getDeduction(), 0.0001);
		assertEquals(38000, workingSheet.getTaxableIncome(), 0.0001);
		assertEquals(2356, workingSheet.getSocialSecurity(), 0.0001);
		assertEquals(551, workingSheet.getMedicare(), 0.0001);
		
		// Married and Low Income
		workingSheet.getRequest().setFilingType(TaxFilingType.MARRIED);
		service.calculate(workingSheet);
		assertEquals(24000, workingSheet.getDeduction(), 0.0001);
		assertEquals(26000, workingSheet.getTaxableIncome(), 0.0001);
		assertEquals(1612, workingSheet.getSocialSecurity(), 0.0001);
		assertEquals(377, workingSheet.getMedicare(), 0.0001);
		
		// Single and High Income
		workingSheet.getRequest().setFilingType(TaxFilingType.SINGLE);
		workingSheet.getRequest().setGrossIncome(200000);
		service.calculate(workingSheet);
		assertEquals(12000, workingSheet.getDeduction(), 0.0001);
		assertEquals(188000, workingSheet.getTaxableIncome(), 0.0001);
		assertEquals(7440, workingSheet.getSocialSecurity(), 0.0001);
		assertEquals(2726, workingSheet.getMedicare(), 0.0001);
		
		// Married and High Income
		workingSheet.getRequest().setFilingType(TaxFilingType.MARRIED);
		workingSheet.getRequest().setGrossIncome(200000);
		service.calculate(workingSheet);
		assertEquals(24000, workingSheet.getDeduction(), 0.0001);
		assertEquals(176000, workingSheet.getTaxableIncome(), 0.0001);
		assertEquals(7440, workingSheet.getSocialSecurity(), 0.0001);
		assertEquals(2552, workingSheet.getMedicare(), 0.0001);
	}
}
