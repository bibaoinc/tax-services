package com.bibao.tax.biz;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import com.bibao.tax.model.StateIncome;
import com.bibao.tax.model.StateTax;
import com.bibao.tax.model.TaxRequest;
import com.bibao.tax.model.TaxWorkingSheet;
import com.bibao.tax.persistence.dao.StateTaxDao;
import com.bibao.tax.persistence.entity.StateTaxEntity;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class StateTaxServiceMockTest {
	@Mock
	private StateTaxDao taxDao;
	
	@InjectMocks
	private StateTaxServiceImpl service;
	
	@Test
	public void testCalcuate() {
		TaxWorkingSheet workingSheet = createWorkingSheet();
		when(taxDao.findByStates(anyList())).thenReturn(createEntities());
		service.calculate(workingSheet);
		
		List<StateTax> stateTaxes = workingSheet.getStateTaxes();
		assertEquals(2, stateTaxes.size());
		double totalStateTax = stateTaxes.stream()
				.collect(Collectors.summingDouble(StateTax::getTax));
		assertEquals(4600, totalStateTax, 0.0001);
	}
	
	private TaxWorkingSheet createWorkingSheet() {
		TaxWorkingSheet workingSheet = new TaxWorkingSheet();
		TaxRequest request = new TaxRequest();
		request.setStateIncomes(new ArrayList<>());
		request.getStateIncomes().add(new StateIncome("NJ", 30000));
		request.getStateIncomes().add(new StateIncome("NY", 40000));
		workingSheet.setRequest(request);
		return workingSheet;
	}
	
	private List<StateTaxEntity> createEntities() {
		StateTaxEntity e1 = new StateTaxEntity();
		e1.setState("NJ");
		e1.setTaxRate(0.06);
		StateTaxEntity e2 = new StateTaxEntity();
		e2.setState("NY");
		e2.setTaxRate(0.07);
		return Arrays.asList(e1, e2);
	}
	
}
