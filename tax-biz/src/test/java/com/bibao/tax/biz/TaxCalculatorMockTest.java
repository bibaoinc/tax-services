package com.bibao.tax.biz;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;

import com.bibao.tax.model.StateIncome;
import com.bibao.tax.model.StateTax;
import com.bibao.tax.model.TaxRequest;
import com.bibao.tax.model.TaxResponse;
import com.bibao.tax.model.TaxWorkingSheet;
import com.bibao.tax.model.enums.TaxFilingType;
import com.bibao.tax.model.exceptions.IllegalIncomeException;
import com.bibao.tax.model.exceptions.NoStateIncomeException;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class TaxCalculatorMockTest {
	@Mock
	private FederalTaxService federalTaxService;
	
	@Mock
	private StateTaxService stateTaxService;
	
	@InjectMocks
	private TaxCalculator service;
	
	@BeforeEach
	public void setup() {
		service.setSocialSecurityService(new SocialSecurityServiceImpl());
	}
	
	@Test
	public void testCalculate() {
		TaxRequest request = buildTaxRequest();
		doMock();
		TaxResponse response = service.calculate(request);
		assertEquals("Alice", response.getName());
		assertEquals("111223333", response.getSsn());
		assertEquals(75000, response.getGrossIncome(), 0.0001);
		assertEquals(5000, response.getFederalTax());
		assertEquals(3500, response.getStateTax());
		assertEquals(3162, response.getSocialSecurity(), 0.0001);
		assertEquals(739.5, response.getMedicare(), 0.0001);
		assertEquals(62598.5, response.getNetIncome()); 
	}
	
	@Test
	public void testCalculateWithBadRequest() {
		// Input is null
		assertThrows(NullPointerException.class, () -> service.calculate(null));
		// Missing TaxFilingType
		TaxRequest request = new TaxRequest();
		assertThrows(NullPointerException.class, () -> service.calculate(request));
		request.setFilingType(TaxFilingType.MARRIED);
		request.setGrossIncome(-10);
		// Invalid grossIncome
		assertThrows(IllegalIncomeException.class, () -> service.calculate(request));
		request.setGrossIncome(80000);
		// No state income is input
		assertThrows(NoStateIncomeException.class, () -> service.calculate(request));
		request.setStateIncomes(new ArrayList<>());
		request.getStateIncomes().add(new StateIncome("NJ", 30000));
	}
	
	private TaxRequest buildTaxRequest() {
		TaxRequest request = new TaxRequest();
		request.setName("Alice");
		request.setSsn("111223333");
		request.setFilingType(TaxFilingType.MARRIED);
		request.setGrossIncome(75000);
		request.setStateIncomes(new ArrayList<>());
		request.getStateIncomes().add(new StateIncome("MD", 30000));
		request.getStateIncomes().add(new StateIncome("NJ", 45000));
		return request;
	}
	
	private void doMock() {
		doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock method) throws Throwable {
				TaxWorkingSheet workingSheet = (TaxWorkingSheet)method.getArguments()[0];
				workingSheet.setFederalTax(5000);
				return null;
			}
		}).when(federalTaxService).calculate(any(TaxWorkingSheet.class));
		
		doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock method) throws Throwable {
				TaxWorkingSheet workingSheet = (TaxWorkingSheet)method.getArguments()[0];
				StateTax md = new StateTax();
				md.setState("MD");
				md.setTax(1500);
				StateTax nj = new StateTax();
				nj.setState("NJ");
				nj.setTax(2000);
				workingSheet.setStateTaxes(Arrays.asList(md, nj));
				return null;
			}
		}).when(stateTaxService).calculate(any(TaxWorkingSheet.class));
	}
}
