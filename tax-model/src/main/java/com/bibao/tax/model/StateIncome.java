package com.bibao.tax.model;

public class StateIncome {
	private String state;
	private double income;
	
	public StateIncome() {}
	
	public StateIncome(String state, double income) {
		this.state = state;
		this.income = income;
	}
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public double getIncome() {
		return income;
	}
	public void setIncome(double income) {
		this.income = income;
	}
}
