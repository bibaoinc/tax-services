package com.bibao.tax.model;

import java.util.List;

import com.bibao.tax.model.enums.TaxFilingType;

public class TaxRequest {
	private String name;
	private String ssn;
	private double grossIncome;
	private List<StateIncome> stateIncomes;
	private TaxFilingType filingType;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public double getGrossIncome() {
		return grossIncome;
	}
	public void setGrossIncome(double grossIncome) {
		this.grossIncome = grossIncome;
	}
	public List<StateIncome> getStateIncomes() {
		return stateIncomes;
	}
	public void setStateIncomes(List<StateIncome> stateIncomes) {
		this.stateIncomes = stateIncomes;
	}
	public TaxFilingType getFilingType() {
		return filingType;
	}
	public void setFilingType(TaxFilingType filingType) {
		this.filingType = filingType;
	}
	@Override
	public String toString() {
		return "TaxRequest [name=" + name + ", ssn=" + ssn + ", grossIncome=" + grossIncome + ", stateIncomes="
				+ stateIncomes + ", filingType=" + filingType + "]";
	}
	
}
