package com.bibao.tax.model;

public class TaxResponse {
	private String name;
	private String ssn;
	private double grossIncome;
	private double federalTax;
	private double stateTax;
	private double socialSecurity;
	private double medicare;
	private double netIncome;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public double getGrossIncome() {
		return grossIncome;
	}
	public void setGrossIncome(double grossIncome) {
		this.grossIncome = grossIncome;
	}
	public double getFederalTax() {
		return federalTax;
	}
	public void setFederalTax(double federalTax) {
		this.federalTax = federalTax;
	}
	public double getStateTax() {
		return stateTax;
	}
	public void setStateTax(double stateTax) {
		this.stateTax = stateTax;
	}
	public double getSocialSecurity() {
		return socialSecurity;
	}
	public void setSocialSecurity(double socialSecurity) {
		this.socialSecurity = socialSecurity;
	}
	public double getMedicare() {
		return medicare;
	}
	public void setMedicare(double medicare) {
		this.medicare = medicare;
	}
	public double getNetIncome() {
		return netIncome;
	}
	public void setNetIncome(double netIncome) {
		this.netIncome = netIncome;
	}
	
}
