package com.bibao.tax.model;

import java.util.List;

public class TaxWorkingSheet {
	private TaxRequest request;
	private double taxableIncome;
	private double deduction;
	private double federalTax;
	private List<StateTax> stateTaxes;
	private double socialSecurity;
	private double medicare;
	
	public TaxRequest getRequest() {
		return request;
	}
	public void setRequest(TaxRequest request) {
		this.request = request;
	}
	public double getTaxableIncome() {
		return taxableIncome;
	}
	public void setTaxableIncome(double taxableIncome) {
		this.taxableIncome = taxableIncome;
	}
	public double getDeduction() {
		return deduction;
	}
	public void setDeduction(double deduction) {
		this.deduction = deduction;
	}
	public double getFederalTax() {
		return federalTax;
	}
	public void setFederalTax(double federalTax) {
		this.federalTax = federalTax;
	}
	public List<StateTax> getStateTaxes() {
		return stateTaxes;
	}
	public void setStateTaxes(List<StateTax> stateTaxes) {
		this.stateTaxes = stateTaxes;
	}
	public double getSocialSecurity() {
		return socialSecurity;
	}
	public void setSocialSecurity(double socialSecurity) {
		this.socialSecurity = socialSecurity;
	}
	public double getMedicare() {
		return medicare;
	}
	public void setMedicare(double medicare) {
		this.medicare = medicare;
	}
}
