package com.bibao.tax.model.enums;

public enum TaxFilingType {
	SINGLE, MARRIED;
	
	public static TaxFilingType from(String input) {
		for(TaxFilingType filingType: values()) {
			if (filingType.name().equalsIgnoreCase(input)) {
				return filingType;
			}
		}
		return null;
	}
}
