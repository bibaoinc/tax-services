package com.bibao.tax.model.exceptions;

public class IllegalIncomeException extends RuntimeException {
	private static final long serialVersionUID = -411199233111519613L;

	public IllegalIncomeException() {
		super();
	}
	
	public IllegalIncomeException(String message) {
		super(message);
	}
}
