package com.bibao.tax.model.exceptions;

public class NoStateIncomeException extends RuntimeException {
	private static final long serialVersionUID = -6628402520599406329L;

	public NoStateIncomeException() {
		super();
	}
	
	public NoStateIncomeException(String message) {
		super(message);
	}
}
