package com.bibao.tax.persistence.dao;

import java.util.List;

import com.bibao.tax.persistence.entity.FederalTaxEntity;

public interface FederalTaxDao {
	public List<FederalTaxEntity> findTaxRatesByType(String taxType);
}
