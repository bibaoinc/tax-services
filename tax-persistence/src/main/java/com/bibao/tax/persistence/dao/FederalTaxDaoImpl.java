package com.bibao.tax.persistence.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.bibao.tax.persistence.entity.FederalTaxEntity;

@Repository
public class FederalTaxDaoImpl implements FederalTaxDao {
	@PersistenceContext(unitName = "JPA_DB")
	private EntityManager em;
	
	@Override
	public List<FederalTaxEntity> findTaxRatesByType(String taxType) {
		TypedQuery<FederalTaxEntity> query = 
				em.createNamedQuery(FederalTaxEntity.FIND_BY_TYPE, FederalTaxEntity.class);
		query.setParameter("taxType", taxType);
		return query.getResultList();
	}

} 
