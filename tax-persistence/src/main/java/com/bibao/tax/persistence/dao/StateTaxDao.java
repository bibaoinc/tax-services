package com.bibao.tax.persistence.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.bibao.tax.persistence.entity.StateTaxEntity;

public interface StateTaxDao extends JpaRepository<StateTaxEntity, String> {
	public StateTaxEntity findByState(String state);
	
	@Query("select e from StateTaxEntity e where e.state in :states")
	public List<StateTaxEntity> findByStates(@Param("states") List<String> states);
}
