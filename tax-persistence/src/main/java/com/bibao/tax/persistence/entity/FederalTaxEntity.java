package com.bibao.tax.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "FEDERAL_TAX")
@NamedQueries({
	@NamedQuery(
		name = FederalTaxEntity.FIND_BY_TYPE,
		query = "select e from FederalTaxEntity e where e.taxType = :taxType"
	)
})
public class FederalTaxEntity implements Serializable {
	private static final long serialVersionUID = 1243884221437848685L;
	
	public static final String FIND_BY_TYPE = "FederalTaxEntity.findTaxRatesByType";

	@Id
	@Column(name = "ID")
	private int id;
	
	@Column(name = "TAX_TYPE")
	private String taxType;
	
	@Column(name = "INCOME_FROM")
	private BigDecimal incomeFrom;
	
	@Column(name = "INCOME_TO")
	private BigDecimal incomeTo;
	
	@Column(name = "TAX_RATE")
	private double taxRate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTaxType() {
		return taxType;
	}

	public void setTaxType(String taxType) {
		this.taxType = taxType;
	}

	public BigDecimal getIncomeFrom() {
		return incomeFrom;
	}

	public void setIncomeFrom(BigDecimal incomeFrom) {
		this.incomeFrom = incomeFrom;
	}

	public BigDecimal getIncomeTo() {
		return incomeTo;
	}

	public void setIncomeTo(BigDecimal incomeTo) {
		this.incomeTo = incomeTo;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	@Override
	public String toString() {
		return "FederalTaxEntity [id=" + id + ", taxType=" + taxType + ", incomeFrom=" + incomeFrom + ", incomeTo="
				+ incomeTo + ", taxRate=" + taxRate + "]";
	}
	
}
