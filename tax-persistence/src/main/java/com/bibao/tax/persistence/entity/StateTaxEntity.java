package com.bibao.tax.persistence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "STATE_TAX")
public class StateTaxEntity {
	@Id
	@Column(name = "STATE")
	private String state;
	
	@Column(name = "TAX_RATE")
	private double taxRate;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	@Override
	public String toString() {
		return "StateTaxEntity [state=" + state + ", taxRate=" + taxRate + "]";
	}
	
}
