package com.bibao.tax.persistence.dao;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.*;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import com.bibao.tax.persistence.entity.FederalTaxEntity;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class FederalTaxDaoMockTest {
	private static final String TAX_TYPE = "MARRIED";
	@Mock
	private EntityManager em;
	
	@InjectMocks
	private FederalTaxDaoImpl dao;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Test
	public void testFindTaxRatesByType() {
		TypedQuery query = mock(TypedQuery.class);
		when(em.createNamedQuery(FederalTaxEntity.FIND_BY_TYPE, FederalTaxEntity.class))
			.thenReturn(query);
		when(query.setParameter(anyString(), any())).thenReturn(query);
		FederalTaxEntity entity = new FederalTaxEntity();
		entity.setIncomeFrom(new BigDecimal(1000));
		entity.setIncomeTo(new BigDecimal(10000));
		entity.setTaxRate(0.1);
		entity.setTaxType(TAX_TYPE);
		when(query.getResultList()).thenReturn(Arrays.asList(entity));
		
		List<FederalTaxEntity> result = dao.findTaxRatesByType(TAX_TYPE);
		assertEquals(1, result.size());
		assertEquals(1000, result.get(0).getIncomeFrom().doubleValue(), 0.0001);
		assertEquals(10000, result.get(0).getIncomeTo().doubleValue(), 0.0001);
		assertEquals(0.1, result.get(0).getTaxRate(), 0.0001);
		assertEquals(TAX_TYPE, result.get(0).getTaxType());
	}
}
