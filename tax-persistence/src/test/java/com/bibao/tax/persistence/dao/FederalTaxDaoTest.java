package com.bibao.tax.persistence.dao;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.bibao.tax.persistence.config.JPATestConfig;
import com.bibao.tax.persistence.entity.FederalTaxEntity;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { JPATestConfig.class })
@ActiveProfiles("test")
public class FederalTaxDaoTest {
	private static final Logger LOG = LoggerFactory.getLogger(FederalTaxDaoTest.class);
	
	@Autowired
	private FederalTaxDao dao;
	
	@Test
	public void testGetTaxRatesByType() {
		List<FederalTaxEntity> entities = dao.findTaxRatesByType("SINGLE");
		assertTrue(CollectionUtils.isNotEmpty(entities));
		assertEquals(6, entities.size());
		entities.forEach(e -> LOG.debug(e.toString()));
	}
}
