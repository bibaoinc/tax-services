package com.bibao.tax.persistence.dao;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.bibao.tax.persistence.config.JPATestConfig;
import com.bibao.tax.persistence.entity.StateTaxEntity;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = { JPATestConfig.class })
@ActiveProfiles("test")
public class StateTaxDaoTest {
	private static final Logger LOG = LoggerFactory.getLogger(StateTaxDaoTest.class);
	
	@Autowired
	private StateTaxDao dao;
	
	@Test
	public void testFindByState() {
		StateTaxEntity entity = dao.findByState("NJ");
		assertNotNull(entity);
		assertEquals(0.06, entity.getTaxRate(), 0.0001);
		assertEquals("NJ", entity.getState());
	}
	
	@Test
	public void testFindByStates() {
		List<String> states = Arrays.asList("TX", "MD");
		List<StateTaxEntity> entities = dao.findByStates(states);
		assertEquals(2, entities.size());
		entities.forEach(e -> LOG.debug(e.toString()));
	}
}
