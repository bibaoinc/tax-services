drop table federal_tax if exists;

create table federal_tax(
	id number(2) primary key,
	tax_type varchar(10) not null,
	income_from number(8),
	income_to number(8),
	tax_rate number(3, 2)
);

insert into federal_tax values(1, 'SINGLE', 0, 9700, 0.1);

insert into federal_tax values(2, 'SINGLE', 9700, 39475, 0.12);

insert into federal_tax values(3, 'SINGLE', 39475, 84200, 0.22);

insert into federal_tax values(4, 'SINGLE', 84200, 160725, 0.24);

insert into federal_tax values(5, 'SINGLE', 160725, 204100, 0.32);

insert into federal_tax values(6, 'SINGLE', 204100, null, 0.35);

insert into federal_tax values(7, 'MARRIED', 0, 19400, 0.1);

insert into federal_tax values(8, 'MARRIED', 19400, 78950, 0.12);

insert into federal_tax values(9, 'MARRIED', 78950, 168400, 0.22);

insert into federal_tax values(10, 'MARRIED', 168400, 321450, 0.24);

insert into federal_tax values(11, 'MARRIED', 321450, 408200, 0.32);

insert into federal_tax values(12, 'MARRIED', 408200, null, 0.35);

drop table state_tax if exists;

create table state_tax(
	state char(2) primary key,
	tax_rate number(3, 2) not null
);

insert into state_tax values('MD', 0.07);

insert into state_tax values('NJ', 0.06);

insert into state_tax values('VA', 0.05);

insert into state_tax values('NY', 0.09);

insert into state_tax values('NC', 0.07);

insert into state_tax values('TX', 0);

insert into state_tax values('PA', 0);
