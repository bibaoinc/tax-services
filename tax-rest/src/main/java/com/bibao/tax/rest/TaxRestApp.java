package com.bibao.tax.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.bibao.tax.rest.config.TaxRestConfig;

@SpringBootApplication
@Import(TaxRestConfig.class)
public class TaxRestApp {
	public static void main(String[] args) {
		SpringApplication.run(TaxRestApp.class, args);
	}
}
