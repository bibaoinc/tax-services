package com.bibao.tax.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.bibao.tax.biz.TaxCalculator;
import com.bibao.tax.model.TaxRequest;
import com.bibao.tax.model.TaxResponse;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/tax")
@Api(value="Tax Services")
public class TaxRestService {
	private static final Logger LOG = LoggerFactory.getLogger(TaxRestService.class);
	@Autowired
	private TaxCalculator service;
	
	@POST
	@Path("/calculate")
	@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@ApiOperation(value = "Standardize Address", response = TaxResponse.class)
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Tax Calculate", response = TaxResponse.class),
			@ApiResponse(code = 400, message = "Bad Request", response = Response.Status.class),
			@ApiResponse(code = 500, message = "Server exception", response = Response.Status.class)})
	public Response calculate(TaxRequest request) {
		LOG.debug("Receiving request: {}", request);
		TaxResponse response = service.calculate(request);
		return Response.ok(response).build();
	}
}
