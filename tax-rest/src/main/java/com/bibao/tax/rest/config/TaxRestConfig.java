package com.bibao.tax.rest.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;

import com.bibao.tax.biz.config.TaxServiceBizConfig;
import com.bibao.tax.persistence.config.JPAConfig;

@Configuration
@ComponentScan(basePackages = "com.bibao.tax.rest")
@Import(value = { JerseyConfig.class, TaxServiceBizConfig.class, JPAConfig.class})
public class TaxRestConfig {
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
	   return builder.build();
	}
}
