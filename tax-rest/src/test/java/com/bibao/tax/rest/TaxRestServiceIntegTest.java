package com.bibao.tax.rest;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Description;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.bibao.tax.model.StateIncome;
import com.bibao.tax.model.TaxRequest;
import com.bibao.tax.model.TaxResponse;
import com.bibao.tax.model.enums.TaxFilingType;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = TaxRestApp.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TaxRestServiceIntegTest {
	private static final String TAX_URL = "/tax-rest/tax/";
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Description("Test method calculate")
	@Test
	public void testCalculate() {
		String url = TAX_URL + "/calculate";
		TaxRequest request = buildTaxRequest();
		ResponseEntity<TaxResponse> exchange = restTemplate.postForEntity(url, request, TaxResponse.class);
		assertEquals(exchange.getStatusCodeValue(), HttpStatus.OK.value());
		TaxResponse response = exchange.getBody();
		assertEquals("Alice", response.getName());
		assertEquals("111223333", response.getSsn());
		assertEquals(75000, response.getGrossIncome(), 0.0001);
		assertEquals(5732, response.getFederalTax(), 0.0001);
		assertEquals(4800, response.getStateTax(), 0.0001);
		assertEquals(3162, response.getSocialSecurity(), 0.0001);
		assertEquals(739.5, response.getMedicare(), 0.0001);
		assertEquals(60566.5, response.getNetIncome(), 0.00001);
	}
	
	private TaxRequest buildTaxRequest() {
		TaxRequest request = new TaxRequest();
		request.setName("Alice");
		request.setSsn("111223333");
		request.setFilingType(TaxFilingType.MARRIED);
		request.setGrossIncome(75000);
		request.setStateIncomes(new ArrayList<>());
		request.getStateIncomes().add(new StateIncome("MD", 30000));
		request.getStateIncomes().add(new StateIncome("NJ", 45000));
		return request;
	}
}
