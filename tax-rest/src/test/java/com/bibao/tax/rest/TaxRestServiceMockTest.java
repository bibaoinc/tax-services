package com.bibao.tax.rest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.client.ExpectedCount.once;
import org.springframework.test.web.client.MockRestServiceServer;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.support.RestGatewaySupport;

import com.bibao.tax.model.TaxRequest;
import com.bibao.tax.model.TaxResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = TaxRestApp.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class TaxRestServiceMockTest {
	private static final String TAX_URL = "/tax-rest/tax/";
	
	@Autowired
	private RestTemplate restTemplate;
	
	private MockRestServiceServer mockServer;
	
	@BeforeEach
	public void setup() {
		RestGatewaySupport gateway = new RestGatewaySupport();
		gateway.setRestTemplate(restTemplate);
		mockServer = MockRestServiceServer.createServer(gateway);
	}
	
	@Test
	public void testCalculate() {
		String url = TAX_URL + "calculate";
		String resp = buildResponse();
		mockServer.expect(once(), requestTo(url)).andExpect(method(HttpMethod.POST))
			.andRespond(withSuccess(resp, MediaType.APPLICATION_JSON));
		
		ResponseEntity<TaxResponse> exchange = restTemplate.postForEntity(url, new TaxRequest(), TaxResponse.class);
		assertEquals(exchange.getStatusCodeValue(), HttpStatus.OK.value());
		TaxResponse response = exchange.getBody();
		assertEquals("Bob", response.getName());
		assertEquals("111557777", response.getSsn());
		assertEquals(80000, response.getGrossIncome(), 0.0001);
		assertEquals(10000, response.getFederalTax(), 0.0001);
		assertEquals(4000, response.getStateTax(), 0.0001);
		assertEquals(3500, response.getSocialSecurity(), 0.0001);
		assertEquals(500, response.getMedicare(), 0.0001);
		assertEquals(62000, response.getNetIncome(), 0.00001);
		
		mockServer.verify();
	}
	
	@Test
	public void testCalculateFail() {
		String url = TAX_URL + "calculate";
		mockServer.expect(once(), requestTo(url)).andExpect(method(HttpMethod.POST))
			.andRespond(withBadRequest());
		
		assertThrows(HttpClientErrorException.class, 
				() -> restTemplate.postForEntity(url, new TaxRequest(), TaxResponse.class)
		);
		
		mockServer.verify();
	}
	
	private String buildResponse() {
		TaxResponse response = new TaxResponse();
		response.setName("Bob");
		response.setSsn("111557777");
		response.setGrossIncome(80000);
		response.setFederalTax(10000);
		response.setStateTax(4000);
		response.setSocialSecurity(3500);
		response.setMedicare(500);
		response.setNetIncome(62000);
		return toJson(response);
	}
	
	private String toJson(Object obj) {
		ObjectMapper mapper = new ObjectMapper();
		String json = null;
		try {
			json = mapper.writeValueAsString(obj);
		} catch(Exception e) {
			// Do nothing
		}
		return json;
	}
}
